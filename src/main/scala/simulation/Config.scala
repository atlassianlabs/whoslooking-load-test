package simulation

object Config {

  val CONNECT_APP = "https://whoslooking-dev.herokuapp.com"
  val POLL_REQUESTS_PER_IFRAME_VIEW = 25
  val NUM_TENANTS = 40
  val NUM_ISSUES_PER_TENANT = 2
  val NUM_USERS_PER_TENANT = 20
  val TOTAL_USERS = NUM_TENANTS * NUM_USERS_PER_TENANT

}