package simulation

import scala.concurrent.duration.DurationInt
import com.atlassian.gatling.jwt.Predef.httpWithJwt

import io.gatling.core.Predef._
import io.gatling.core.Predef._
import io.gatling.core.Predef.assertions.global
import io.gatling.core.Predef.bootstrap.{exec, feed}
import io.gatling.http.Predef._
import simulation.Config._
import simulation.Feeders._

/**
 * An example simulation against a Connect app.
 *
 * IMPORTANT: run the TenantRegistrationSimulation at least once first, so that the mock tenants that will be used during the load test are registered against your tenant.
 */
class WhosLookingSimulation extends Simulation {

  val httpProtocol = http.baseURL(CONNECT_APP)

  val scn = scenario("A mixture of iframe and XHR requests")
    .during(20 minutes) {
      exec(
        // Prepare the session with a tenant key, user key and issue key picked at random.
        feed(tenantKeyFeeder.random)
          .feed(userKeyFeeder.random)
          .feed(issueKeyFeeder.random)
          .exec(
            // Issue a JWT-signed request to the poller iframe.
            httpWithJwt("iframe-request")
              .get("/poller")
              .queryParam("issue_key", "${issue}")
              .queryParam("user_key", "${user}")
              .queryParam("user_id", "${user}")
              .queryParam("tz", "Australia/Sydney")
              .queryParam("loc", "en-US")
              .sign("${tenant}", "${user}", "mock-tenant-shared-secret")
              .check(status.is(200),
                // Collect the page-view token so we can simulate poll requests.
                css("meta[name=acpt]", "content").saveAs("acpt-token")))
          .repeat(POLL_REQUESTS_PER_IFRAME_VIEW) {
            exec(
              // Simulate XHR poll requests
              http("token-based-xhr-request")
                .put("/viewables/${tenant}/${issue}/viewers/${user}")
                .header("X-acpt", "${acpt-token}")
                .check(status.is(200), header("X-Acpt").saveAs("acpt-token")))
          })
    }

  setUp(scn.inject(ramp(TOTAL_USERS users) over (10 minutes)))
    .protocols(httpProtocol)
    .assertions(global.successfulRequests.percent.is(100))
}
