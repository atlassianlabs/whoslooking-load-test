package simulation

import scala.concurrent.duration.DurationInt

import com.atlassian.gatling.jwt.Predef.httpWithJwt

import io.gatling.core.Predef._
import io.gatling.core.Predef._
import io.gatling.core.Predef.assertions.global
import io.gatling.core.Predef.bootstrap.{ exec, feed }
import io.gatling.http.Predef._
import simulation.Config._
import simulation.Feeders._

/**
 * Registers a bunch of tenants with your application.
 *
 * The registered tenants will have keys like 'tenant-1', 'tenant-2'...
 * You can customize the details (such as application type, base url etc...) by modifying registration-template.json.
 *
 * By default, the tenants' base URLs will be http://ac-mock-tenants.herokuapp.com/jira/${tenant}.
 *
 */
class TenantRegistration extends Simulation {

  val httpProtocol = http.baseURL(CONNECT_APP)

  val registerHostsScenario = scenario(s"Register ${NUM_TENANTS} Tenants")
    .feed(tenantKeyFeeder)
    .exec(http("register-tenant")
      .post("/installed")
      .body(ELFileBody("registration-template.json")).asJSON
      .check(status.is(200)))
      .pause(1 second)

  setUp(registerHostsScenario.inject(ramp(NUM_TENANTS users) over (10 seconds)))
    .protocols(httpProtocol)
    .assertions(global.successfulRequests.percent.is(100))

}
