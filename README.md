# Gatling Load Tests for Who's Looking

Gatling-based Load tests for the Atlassian Connect add-on "[Who's Looking](https://bitbucket.org/atlassian/whoslooking-connect)", built using the [ac-load-test.g8 template](https://bitbucket.org/atlassianlabs/ac-load-test.g8/overview).