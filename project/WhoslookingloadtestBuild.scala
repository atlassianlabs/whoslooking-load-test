import sbt._
import sbt.Keys._

object WhoslookingloadtestBuild extends Build {

  lazy val whoslookingloadtest = Project(
    id = "whoslooking-load-test",
    base = file("."),
    settings = Project.defaultSettings ++ Seq(
      resolvers += "Gatling repo" at "http://repository.excilys.com/content/groups/public",
      resolvers += "Atlassian's Maven Public Repository" at "https://maven.atlassian.com/content/groups/public",
      resolvers += "Local Maven Repository" at "file://" + Path.userHome + "/.m2/repository",
      name := "whoslooking-load-test",
      organization := "com.example",
      version := "0.1-SNAPSHOT",
      scalaVersion := "2.10.3",
      libraryDependencies += "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.0.0-M3a",
      libraryDependencies += "io.gatling" % "gatling-app" % "2.0.0-M3a",
      libraryDependencies += "com.atlassian.gatling" %% "gatling-jwt" % "0.1"))
}
